from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages


# Ce je bila registracijska forma izpolnjena pravilno, uporabnika shranimo, mu sporocimo da je bil uspesno registriran
# ter ga preusmerimo na prijavno stran. Drugace pa samo kreiramo novo formo katero bomo posredovali serverju nazaj, ko
# se bo uporabnik hotel registrirati.
def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        data = {
            'form': form,
            'title': "Registration",
        }
        if form.is_valid():
            form.save()
            messages.success(request, f'Account has been created. You can now login.')
            return redirect('login')
    else:
        form = UserCreationForm()
        data = {
            'form': form,
            'title': 'Registration'
        }
    return render(request, 'users/html/register.html', data)