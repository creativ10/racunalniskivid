from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


# kreiranje Modela ki ga vpisemo v bazo
class Slike(models.Model):
    location = models.CharField(max_length=50)
    name = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='uploads')
    data = models.TextField(default="")

    def __str__(self):
        return "Lokacija: " + self.location + "\n" + "Author: " + self.author.username

    # metoda za kreiranje novega objekta Slike, na podlagi podanih parametrov
    @classmethod
    def create(cls, data, image, location, author):
        slika = cls(data=data, image=image, location=location, author=author)
        return slika