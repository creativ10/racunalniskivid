from django import forms

from .models import Slike


# override forme, preko katere bomo vpisali/naloziili podatke v bazo
class SlikaUploadForm(forms.ModelForm):

    class Meta:
        model = Slike
        fields = ['location', 'image', 'author', 'data']
        widgets = {
            'author': forms.HiddenInput(),
            'data': forms.HiddenInput()
        }
