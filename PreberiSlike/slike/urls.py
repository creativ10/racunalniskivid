from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='slike-home'),
    path('PreviousUploads/', views.PreviousUploads, name='slike-PreviousUploads')
]