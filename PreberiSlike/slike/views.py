from django.shortcuts import render, redirect
from .models import Slike
from django.contrib.auth.decorators import login_required
from .forms import SlikaUploadForm
from django.contrib import messages
import cv2 as cv
import numpy as numpy
import pytesseract as pt
import re


def home(request):
    if request.method == "GET":
        slike_form = SlikaUploadForm(initial={
            'author': request.user,
            'data': "temp"
        })
        data = {
            'title': 'Home Page',
            'slike_form': slike_form,
        }
        return render(request, 'slike/html/Home.html', data)
    elif request.method == "POST":
        slike_form = SlikaUploadForm(request.POST, request.FILES)
        if slike_form.is_valid():
            # beremo sliko
            img = cv.imdecode(numpy.fromstring(request.FILES['image'].read(), numpy.uint8), cv.IMREAD_UNCHANGED)
            # pretvorba v sivinsko
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            # dobimo width,height slike
            (height, width) = img.shape
            # odrezemo polovico slike (1/4 zgoraj, 1/4 spodaj)
            img = img[height // 4:height - height // 4, :]
            # naredimo thresholding
            r, img = cv.threshold(img, 127, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)

            # closing in opening nad sliko, da se znebimo majhnih artefaktov
            kernel = numpy.ones((3, 3), numpy.uint8)
            img = cv.morphologyEx(img, cv.MORPH_OPEN, kernel)
            img = cv.morphologyEx(img, cv.MORPH_CLOSE, kernel)

            # Preberemo text iz slike
            ret = pt.image_to_string(img)
            ret = re.split("[ \n]+", ret)
            data = ""
            # s pomocju regexa sestavimo niz primeren za vpis v bazo
            for i in ret:
                if re.search("(\w{4,}|\d+% |\d+,?\d?L)", i):
                    data += " " + i
            s = Slike.create(data, slike_form.cleaned_data['image'], slike_form.cleaned_data['location'], request.user)
            s.save()
            # plt.imshow(img)
            # plt.show()
            messages.success(request, f'Picture has been uploaded!')
            return redirect("slike-home")
        print(slike_form.errors)


@login_required
def PreviousUploads(request):
    data = {
        'title': 'Previous Uploads',
        'AllUploads': Slike.objects.filter(author=request.user),
    }
    return render(request, 'slike/html/PreviousUploads.html', data)
